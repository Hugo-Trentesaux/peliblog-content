Title: Hello World !
Date: 2017-05-08 00:20
Tags: pelican, markdown
Slug: hello-world
Summary: Pelican, générateur de site statique, et Abricotine, éditeur visuel markdown confortable.

Bonjour !
Aujourd'hui, nous venons d'élire Macron président. Mais ce n'est pas de ça que je veux parler. À vrai dire, ça ne m'intéresse pas beaucoup. Non, aujourd'hui, j'ai décidé de commencer un blog, pour noter les idées qui me passent par la tête et parce que c'est satisfaisant.

Tout d'abord, le choix des outils, parce qu'en tant que geek, j'y porte beaucoup d'attention et j'y consacre beaucoup de temps.

## Pelican

Pelican ([https://blog.getpelican.com/](https://blog.getpelican.com/)) est un générateur de site web statique à partir de simples textes markdown. C'est un outil que j'ai découvert il y a longtemps sans jamais l'utiliser. Je suis ravi de l'avoir gardé dans un coin de ma tête, parce que je sens qu'aujourd'hui, il va me simplifier la vie. J'ai déjà commencé mon site personnel en django, plus pour apprendre que parce que j'en ai vraiment besoin, mais pour un blog, je n'avais pas besoin d'une telle arme ! Voilà donc un blog généré par Pelican (en plus le nom est cool).

## Abricotine

Le [markdown](https://daringfireball.net/projects/markdown) est certainement une des syntaxes les plus utilisées par les geeks. Mais elle pourrait être utilisée par tout le monde : c'est tellement simple et pratique ! Du coup, l'internet regorge d'éditeurs markdown tous différents les uns des autres, impossible de choisir ! J'ai finalement opté pour Abricotine ([https://abricotine.brrd.fr/](http://abricotine.brrd.fr/)) parce qu'il est très confortable et remplit bien mes besoins.