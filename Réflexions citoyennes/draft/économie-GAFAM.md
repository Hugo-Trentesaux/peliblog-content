Title: Comment les GAFAM vampirisent notre économie
Date: 2017-12-31
Modified: 2017-12-31
Category: Informatique Durable
Tags: GAFAM
Slug: economie-vs-GAFAM
Authors: Hugo Trentesaux
Summary: Les GAFAM, une bande d'entreprises géantes de la silicon valley, pratiquent une optimisation fiscale massive. Celle-ci n'est pas sans effet sur notre économie.
Status: draft

- les GAFAM se sont à elles seules emparées d'un secteur de marché en pleine expansion
	- passage de la révolution industrielle à la révolution informatique
	- marché de la publicité déplacé en ligne, accaparé par quelques acteurs
	- services informatisés gratuits
- ces pieuvres génèrent un bénéfice tentaculaire et le protègent des taxes étatiques par des montages fiscaux sophistiqués
	- un point sur les chiffres d'affaire
	- montage financier des irlandais
	- les pyramides (booking.com / google)
- la plupart des secteurs, profondément touchés par cette "robotisation" de la tache, sont impactés
	- réservation en ligne, comparateurs de prix
	- le manque généré se fait ressentir sur l'emploi
- les nouveaux secteurs fonctionnent sur le même modèle
	- l'apparition de l'informatique a permis uber, airbnb, locations de voiture, covoiturage...
	- quasiment tous sont non éthiques
- les GAFAM prennent le rôle de l'état (donner accès à internet) sans le prendre en entier (éducation, santé...)




- pierre musso, féodalité industrielle

- plus loin que les GAFAM, la collecte des données (compagines d'assurances...) 