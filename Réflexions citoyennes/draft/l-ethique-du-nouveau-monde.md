Title: L'éthique du nouveau monde
Date: 2017-12-31
Modified: 2017-12-31
Category: Informatique durable
Tags: éthique
Slug: l-ethique-du-nouveau-monde
Authors: Hugo Trentesaux
Summary: L'informatique a profondément bouleversé notre société. Plusieurs aspects qui ne choquent plus aujourd'hui sont contre toute éthique pour les années passées.
Status: draft

- surveillance systématique des conversations
- gratuité
- abondance d'information
- exposition de la vie privée
- instantanéité
- délégation de la pensée / mémoire (GPS / Wikipedia)
- disparition du choix, de la diversité, et en même temps explosion des possibles
- monopoles planétaires
- communication mondiale