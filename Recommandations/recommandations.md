Title: Recommandations
Date: 2018-05-19
Authors: Hugo Trentesaux
Summary: Une liste de recommandation diverses de lecture / visionnage organisée par thèmes.
Status: draft

Ça fait un moment que je voulais ouvrir une section de recommendations pour partager les sources qui ont nourri mon inspiration (livres, vidéos, articles). La voilà enfin ! elle sera organisée de manière thématique pour une navigation facile. Cet article facilitera la navigation parmi les différentes pages.