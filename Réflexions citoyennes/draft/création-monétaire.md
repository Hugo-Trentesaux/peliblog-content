Title: Création monétaire
Date: 2018-05-01
Authors: Hugo Trentesaux
Summary: Des idées sur la création monétaire.
Status: draft

Économistes :
- Bertrand Séné (http://bertrandsene.fr/)
- Ann Pettifor
- Philippe Derudder
- Michel Laloux
- Stéphane Laborde

Projets :
- monnaie démocratique
- https://climat-2020.eu/