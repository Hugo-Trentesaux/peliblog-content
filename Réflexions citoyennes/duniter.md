Title: Duniter, pour une monnaie libre
Date: 2018-02-04
Tags: duniter, monnaie-libre
Slug: duniter
Summary: Notre système économique actuel ne convient pas à beaucoup d'entre nous. Duniter est une cryptomonnaie "libre" qui pourrait nous aider à construire une économie qui nous ressemble plus.

Le système monétaire actuel a des limites que nous subissons régulièrement. Entre les crises financières, l'emprise des banques sur l'économie et la dette absurde des états, l'outil "monnaie" dont nous avons hérité n'est pas adapté aux problématiques d'aujourd'hui.
C'est dans ce contexte que l'ingénieur Stéphane Laborde, s'est posé la question suivante : est-il théoriquement possible de construire un système monétaire qui n'introduise pas d'inégalités ? Dans son livre [_Théorie relative de la monnaie_](http://trm.creationmonetaire.info/), il construit les principes fondamentaux qui garantissent l'égalité des hommes vis-à-vis d'une "monnaie libre". J'introduis ici duniter, un logiciel implémentant les principes d'une "monnaie libre", et ğ1, la première cryptomonnaie libre en production à l'utiliser.

## La création monétaire

### En euro

Rappelons rapidement comment fonctionne le système monétaire actuel. La monnaie centrale euro est créée ou détruite par la Banque Centrale européenne. Sa quantité est contrôlée de manière centralisée et dépend de choix politiques de l'Europe (plus de détails sur [le site création monétaire](http://www.creationmonetaire.info/2016/01/masse-monetaire-e-janvier-2016-la-bce-credite-en-hyper-accelere.html)). Quand des banques de la zone euro émettent des crédits, elles créent une monnaie dette à valeur d'euro. La loi limite cette création monétaire à une valeur dix fois supérieure à la monnaie centrale possédée par la banque, mais cette limite n'est pas respectée : les grandes banques font elles-mêmes leurs lois. Quand l'emprunt est remboursé, la banque supprime le crédit émis, et y gagne les intérêts. On est donc en présence d'une "monnaie dette".

Pour obtenir un crédit auprès d'une banque, il faut que celle-ci ait confiance dans notre capacité à le rembourser. Si elle n'a pas confiance, elle n'accorde pas de crédit. Que ce crédit s'accompagne ou pas d'une création de valeur, il est accordé suivant les critères de la banque. En fait, c'est la banque qui discrimine les projets qui pourront donner lieu à création de valeur. Il existe des banques comme [la Nef](https://www.lanef.com/) ou le [crédit coopératif](http://www.credit-cooperatif.coop/) qui choisissent des projets selon des critères éthiques, mais [d'autres](https://fr.wikipedia.org/wiki/HSBC#Scandales) financent tout ce qui rapporte.

### En monnaie libre

Dans une monnaie libre, on ne veut pas que la monnaie induise ce type d'inégalités. Dans un système de crédit, il faudrait que chacun puisse décider de ce qui représente une création de valeur, et qu'on puisse par exemple accorder un crédit de manière démocratique. C'est impossible, car il suffit qu'une personne de mauvaise volonté crée de la monnaie sans limite pour que le système entier soit affaibli. La solution adoptée est que la création monétaire soit répartie dans le temps et entre tous les participants de la monnaie. Par exemple, chaque jour, chaque personne reçoit une part de création monétaire définie à l'avance ; on appellera cela un "dividende universel", un DU. Ainsi, chacun décide ce qui représente pour lui une création de valeur. Il est évidemment possible de contracter une dette en empruntant de l'argent auprès de quelqu'un d'autre, mais cet argent n'est pas le résultat d'une spéculation sur la capacité d'une entreprise à créer de la valeur (ou simplement des revenus), elle a été générée en partant du principe que chacun était capable de créer de la valeur et devait en avoir l'opportunité.

## L'implémentation duniter

### La toile de confiance

Ce principe de création monétaire paraît simple, en réalité, il est difficile de s'assurer qu'une personne va recevoir une seule part de création monétaire. Pour cela, des collectifs construisent une "toile de confiance" (web of trust). Chaque personne qui veut créer de la monnaie doit être certifiée par au moins cinq membres de la monnaie. Ainsi, les membres peuvent attester de leur confiance : l'individu est-il bien humain ? Ne tente-t-il pas de créer plusieurs identités ? De proche en proche, tout le monde peut intégrer la toile de confiance et donc participer à la création monétaire.

### Une cryptomonnaie

Maintenant que chaque personne peut créer de la monnaie, il faut quand même tenir les livres de compte. Où va-t-on écrire qui possède combien ? On ne veut pas confier cette responsabilité à un petit groupe de personnes : elles risqueraient de changer des lignes de compte à leur avantage. La solution adoptée repose sur la blockchain. Cet outil a été popularisé par le bitcoin, la première monnaie à l'utiliser dans ce but. La blockchain permet de contrôler l'évolution du livre de comptes de manière décentralisée et sécurisée. Ainsi, chacun peut prêter son ordinateur pour participer à tenir les comptes, mais personne ne peut les modifier sans que tout le monde ne s'en aperçoive, et les modifications ne seront alors pas prises en compte. Le bitcoin, pour encourager les ordinateurs à participer, récompensent leurs propriétaires d'une part de création monétaire. C'est là son principe. Malheureusement, cela a conduit à une course à la puissance de calcul, qui a entraîné une sorte de catastrophe écologique : le bitcoin consomme aujourd'hui plus de 1% de la puissance électrique mondiale ([ref][1]). Duniter n'a pas ce problème ([ref][2]).

## Ğ1, première monnaie en production

Une fois le logiciel construit, on peut l'utiliser pour créer autant de monnaies que l'on veut. La première monnaie créée s'appelle ğ1. Elle date de mars 2017 et se propage actuellement dans toute la France. Elle compte à ce jour (4 février 2018) 666 membres (et je n'ai même pas fait exprès ! [ref][3]) répartis dans toute la France ([ref][4]).
Des collectifs se mettent en place pour faire vivre cette monnaie, j'ai participé à [celui d'Île-de-France](https://iledefrance.monnaie-libre.fr/). Vous pouvez consulter [le site de la monnaie libre](https://monnaie-libre.fr/) pour découvrir ce qu'il existe près de chez vous !

## Liens utiles

- [site du logiciel duniter][6]
- [carte des membres de la ğ1][4]
- [carte des groupes de monnaie libre][5]
- [visualisation de la toile de confiance][10]
- [statistiques sur le nombre de membres][3]
- [lien pour créer un compte][7]
- [site d'échange en ğ1][8]
- [forum sur le thème de la monnaie libre][9]


[1]: https://digiconomist.net/bitcoin-energy-consumption "Digiconomist"
[2]: https://duniter.org/fr/duniter-est-il-energivore/ "Duniter énergivore ?"
[3]: https://g1.duniter.fr/#/app/currency/lg "Évolution du nombre de membres"
[4]: https://g1.duniter.fr/#/app/wot/map "Carte des membres"
[5]: https://framacarte.org/en/map/duniter-g1_8702#6/47.488/6.449 "framacarte"
[6]: https://duniter.org/fr/ "site du logiciel duniter"
[7]: https://g1.duniter.fr/#/app/home "césium"
[8]: https://www.gchange.fr/#/app/home "ğchange"
[9]: https://forum.monnaie-libre.fr "forum de monnaie libre"
[10]: https://duniter.normandie-libre.fr/wotmap/

[//]: # "système des réserves fractionnaires"

