Title: Chaînes youtube (plutôt informatives)
Date: 2018-09-23
Authors: Hugo Trentesaux
Summary: Une liste des chaînes youtube que je recommande.
Status: draft

<style>
    img {
        width: 64px;
        float: left;
        margin-top: 1em;
        margin-right: 1em;
        margin-bottom: 1em;
        }
    h3 {
        margin-top: 2.5em;
        }
    p {
        clear: both;
        }
</style>

# À propos de youtube

Un petit mot à propos de youtube : je n'encourage pas l'utilisation de cette plateforme de Google, qui surveille ses utilisateurs et dont le modèle économique pousse les créateurs à la censure. Mais c'est pour l'instant une plateforme qui regroupe un certain nombre de contenu en exclusivité.

## Meta : vidéos sur youtube

Faîtes une petite [recherche sur youtube avec le mot clé "adpocalypse"](https://www.youtube.com/results?search_query=adpocalipse), vous trouverez beaucoup de vidéos critiquant le modèle économique de YouTube, parmi lesquelles je recommande :

- [The Adpocalypse: What it Means](https://youtu.be/Z7M7yyRDHGc) (en)

Deuxième recherche, cette fois-ci [à propos du "content ID"](https://www.youtube.com/results?search_query=content+ID), l'outil automatique qui détecte le copie et supprime les vidéos automatiquement. Je recommande :

- [Culture Tube - Content ID](https://youtu.be/fOUxZqsvb64) (fr)


## Vulgarisation scientifique

![img](https://yt3.ggpht.com/a-/AN66SAzbKrxzbVJ4lGgtXzJeaCMYnkChmat6AGyAKA=s288-mo-c-c0xffffffff-rj-k-no)
### Science étonnante

[Science étonnante](https://www.youtube.com/user/ScienceEtonnante) (fr) est ma chaîne de vidéo préférée. Elle compte plusieurs playlist parmi lesquelles :

- [plutôt physique](https://www.youtube.com/playlist?list=PLxzM9a5lhAulfN_MvIJOE0RfNmHeygRtz)
- [plutôt maths](https://www.youtube.com/playlist?list=PLxzM9a5lhAuk-LctI0hTARO2yIMJtvPgJ)
- [plutôt bio](https://www.youtube.com/watch?v=yd_bfJ9kpPA&list=PLxzM9a5lhAukklYDYOVUqB7bsCeD0YA3r)
- ["crétins de cerveau" (sciences sociales)](https://www.youtube.com/playlist?list=PLxzM9a5lhAumFRpcigmGY1QLDYxb4-P2B)

Les vidéos durent en général entre 15 et 30 minutes. Mes préférées sont :

- [musique et maths](https://youtu.be/cTYvCpLRwao)

![img](https://yt3.ggpht.com/a-/AN66SAzNFLzmZsv-CY0Qn5hlYD2ZPj27uTli4MPTvQ=s288-mo-c-c0xffffffff-rj-k-no)
### La statistique expliquée à mon chat

[La statistique expliquée à mon chat](https://www.youtube.com/channel/UCWty1tzwZW_ZNSp5GVGteaA) (fr), des chouettes petites vidéos suffisamment simples pour être comprises par tous. Mes préférées :

- [Monsieur le président, avez-vous vraiment gagné cette élection ?](https://www.youtube.com/watch?v=vfTJ4vmIsO4)

![img](https://yt3.ggpht.com/a-/AN66SAyEdxfzy3VccfFckGQN4qKgGzhXmLldUtanMA=s288-mo-c-c0xffffffff-rj-k-no)
### Hygiène Mentale

[Hygiène Mentale](https://www.youtube.com/channel/UCMFcMhePnH4onVHt2-ItPZw) (fr) fait de la "zététique", c'est à dire la méthode de raisonnement rationnel scientifique. Du contenu de très bonne qualité.

## Vulgarisation économique 

![img](https://yt3.ggpht.com/a-/AN66SAxipMRPKk3pHHbiF7kXP5kyiNe9Nq0lMOMmzQ=s288-mo-c-c0xffffffff-rj-k-no)
### Stupid Economics
[Stupid Economics](https://www.youtube.com/channel/UCyJDHgrsUKuWLe05GvC2lng) (fr) est très bien pour se divertir et éveiller sa curiosité. Des vidéos plutôt courtes et toniques, plutôt superficielle, mais très bien montées. Je recommande :

- [Comment cuire des nouilles au barbecue impacte l’environnement #Externalités](https://youtu.be/Uz0kLni4ZgU)
- [Recycler et gagner du $ en Finlande](https://youtu.be/63BurtaCXyg)
- [NOTRE ARGENT : On vous dit tout !](https://youtu.be/8uhDig2lg9s)

![img](https://yt3.ggpht.com/a-/AN66SAzIaypUqMmbtwl68KVW6cct1dVqdSrn3A0q5g=s288-mo-c-c0xffffffff-rj-k-no)
### Heu?reka
[Heu?reka](https://www.youtube.com/channel/UC7sXGI8p8PvKosLWagkK9wQ) (fr) propose des vidéos un peu plus longues sous la forme de cours. Il faut s'accrocher pour comprendre, mais ça en vaut la peine ! Mes préférées :

- [Actionnaires VS Dirigeants : ce que veulent les entreprises](https://youtu.be/aIOUlRtCDKk)
- [Monnaie pleine : réformer la création monétaire ?](https://youtu.be/nHnH-a9ySgY)
- [Playlist : la crise de l'euro](https://www.youtube.com/playlist?list=PLbHgDl1izvblo60rgF3r-gmq5ttWq6CCt)
- [Playlist : les subprimes](https://www.youtube.com/playlist?list=PLbHgDl1izvblls0OPWuB8WYDTo4AP8f5b)



## Divers

![img](https://yt3.ggpht.com/a-/AN66SAwpg4QigMs4ELwOEFGwTitboBEok9F7J5KQLQ=s288-mo-c-c0xffffffff-rj-k-no)
### Data Gueule
[Data Gueule](https://www.youtube.com/channel/UCm5wThREh298TkK8hCT9HuA) (fr) des vidéos informatives sur plein de sujets, avec plein de chiffres et un bon rythme. Quelques documentaires très bien faits.

Data Gueule a aussi un [chaîne peertube](https://peertube.datagueule.tv/). Pour regarder sans être surveillé !

![img](https://yt3.ggpht.com/a-/AN66SAxeYVHfTgWOikxdmkMsJw-fyDAr9oCIIztY=s288-mo-c-c0xffffffff-rj-k-no)
### Tu mourras moins bête
[Tu mourras moins bête](https://www.youtube.com/channel/UCKtG_lXZk4pRJkapfK0eprA) est une chaîne d'Arte très divertissante.

![img](https://yt3.ggpht.com/a-/AN66SAwiQ72AzGd3PZK3PZ4dtxL4Vd1p5BGe0isdng=s288-mo-c-c0xffffffff-rj-k-no)
### Les clés des médias
[Les clés des médias](https://www.youtube.com/channel/UC29yWVXoLnOhSJ1G1AFMGnw) (fr) propose des petites vidéos éducatives sur la presse et la chaîne de l'information. Très bien pour regarder avec des enfants !

![https://yt3.ggpht.com/a-/AN66SAyPIRXkzj2zqWTRnfYBLCxfkQMb2o7jnL6mjw=s288-mo-c-c0xffffffff-rj-k-no](https://yt3.ggpht.com/a-/AN66SAyPIRXkzj2zqWTRnfYBLCxfkQMb2o7jnL6mjw=s288-mo-c-c0xffffffff-rj-k-no)
### Kurtzgesagt
[Kurzgesagt – In a Nutshell](https://www.youtube.com/user/Kurzgesagt) (en) des vidéos en infographie sur plein de sujets, plutôt divertissant.
