Title: Une monnaie démocratique ?
Slug: monnaie-democratique
Tags: démocratie, monnaie-libre
Date: 2018-08-25
Authors: Hugo Trentesaux
Summary: Qu'est-ce qu'une monnaie démocratique ? Quelle forme prend-elle ? Introduction et résume de la théorie relative de la monnaie.

Ma réflexion sur la monnaie a commencé en septembre 2017, alors que je rencontrais pour la première fois le collectif de "monnaie libre" d'Île-de-France. J'ai écrit en février 2018 [un article](https://blog.trentesaux.fr/duniter.html) sur l'avancement de ma pensée à ce sujet que j'aimerais aujourd'hui compléter et approfondir.

## Le monde d'aujourd'hui est façonné par la finance

La monnaie a toujours été l'instrument essentiel du pouvoir : l'Église catholique n'aurait pas construit des cathédrales sans prélever la dîme, la guerre de Cent Ans n'aurait pas eu lieu sans prélever la taille. Payer revient à exercer un pouvoir, que ce soit dans un but constructif ou destructif. Mais si le monde d'autrefois était façonné par l'agriculture et la religion puis par l'industrie et la politique, le monde d'aujourd'hui est clairement façonné par la finance et la publicité.

Des entreprises comme les <abbr title="(Google, Apple, Facebook, Amazon, Microsoft)">GAFAM</abbr>, AirB&B, Starbucks ou Booking.com transforment radicalement notre organisation sociale en s'appuyant sur leur spéculation en bourse, et c'est aussi le cas pour des entreprises comme Uber et Snapchat qui fonctionnent depuis plusieurs années sans même être rentables. Le poids économique de ces géants dépasse celui de grands pays. Ils pratiquent tous une *optimisation fiscale* agressive qui leur évite de payer des impôts. Le manque à gagner pour un État comme la France se compte en dizaines de milliards d'euros annuels.
Mais les États ne subissent pas que des effets indirects de la finance mondiale : ils s'endettent eux mêmes auprès des marchés financiers. Le coût annuel de cette dette se compte aussi en dizaines de milliards d'euros. Par comparaison le "trou de la sécu" est dix fois inférieur. L'État n'a plus de pouvoir, il subit la domination de la finance.

La finance est donc aujourd'hui l'instrument ultime de domination, et les États, incapables de remettre en cause ce système, se contentent de réduire leur budget. C'est pourquoi nous assistons à la destruction progressive du service public.

*Je pourrais rentrer dans le détail et parler de la Grèce, de L'Argentine, du fonctionnement de l'Euro, des accords de Bâle ou de la crise de 2008, mais cela prendrait beaucoup de pages. Je vous conseille quand même de vous renseigner sur le fonctionnement de l'Euro pour lire le paragraphe suivant sans trop d'idées reçues sur le fonctionnement actuel d'une monnaie.*


## La théorie relative de la monnaie

C'est dans ce contexte d'impuissance face à la finance que beaucoup de citoyens questionnent le fonctionnement de la monnaie. Que ce soient les monnaies locales complémentaires (MLC), les diverses "cryptomonnaies", les mouvements comme "Quantitative Easing for people", tous essayent de trouver une solution au problème. La [théorie relative de la monnaie](http://trm.creationmonetaire.info/) (TRM), publiée en 2012 propose une solution théorique très pertinente que je vais résumer ici. Si mon résumé vous semble imprécis, consultez le lien vers le chapitre concerné de la TRM.

### Définitions [(source)][définitions]

#### Zone monétaire
La TRM se définit dans un repère appelé *zone monétaire*. C'est l'invariant de toute société humaine : elle est constituée d'un ensemble d'individus se renouvelant perpétuellement (naissances et décès) qui échangent des biens et services résultat de leur production.

En première approximation, on considère que l'espérance de vie (notée $\text{ev}$) des individus de la zone monétaire est à peu près égale et que la démographie (la quantité d'individus vivant, notée $N$) évolue peu (à peu près autant de naissances que de décès).

#### Monnaie
Une monnaie, définie comme intermédiaire d'échange, sert alors de *mesure de valeur* universelle dans cette zone monétaire. La valeur est subjective et propre à chaque individu, mais quand un échange a lieu, le prix, ou mesure de valeur, est objectif. La monnaie permet donc de passer d'une mesure de valeur subjective à une mesure de valeur objective. La quantité totale de monnaie est appelée *masse monétaire* (notée $M(t)$).
Il peut exister plusieurs monnaies (plusieurs intermédiaires d'échange) qui peuvent être imposés ou choisies de manière démocratique.

#### Symétrie
Une monnaie respectant le principe de symétrie n'introduit pas d'inégalités entre les individus de la zone monétaire. C'est à la fois une symétrie spatiale et temporelle : aucun individu n'est privilégié vis-à-vis de la création monétaire par rapport à un autre individu contemporain ou futur. Ce principe est présent dans la Déclaration des Droits de l'Homme et donc dans la Constitution française. Il n'est pas respecté dans l'Euro car une banque est privilégiée par son pouvoir de création monétaire (émission de crédit).

#### Unité relative et pouvoir
Un certain montant de monnaie n'a de sens que relativement à la quantité totale de monnaie. On appelle "pouvoir" (noté $P$) la part que représente ce montant. Par exemple 1 unité a moins de pouvoir s'il y a 100 unités en circulation que s'il y en a 10. On parle de plein pouvoir ou de pouvoir moyen pour $P=M/N$.

#### Dividende universel
Un dividende universel (noté DU) est une part de création monétaire versé de manière égale à tous les individus d'une zone économique. L'augmentation de la masse monétaire est divisée en part égale entre tous les individus de la zone monétaire.

### Théorème [(source)][solutions]
> Une monnaie qui respecte le principe de symétrie doit être créée sous forme d'un dividende universel et la masse monétaire $M$ doit évoluer suivant la loi exponentielle :

\begin{equation}\label{exponentielle} M(t) = M(0)e^{ct} \end{equation}

#### Démonstration
Avec $m(x,t)$ la monnaie d'un individu $x$ (modèle continu) à l'instant $t$, la création monétaire pour l'individu vaut :
$$ \mathrm{d}m = \frac{\partial m}{\partial x}\mathrm{d}x + \frac{\partial m}{\partial t}\mathrm{d}t $$
La symétrie spatiale implique
$$ \frac{\partial m}{\partial x} = 0 $$
donc $\mathrm{d}m$ ne dépend pas de $x$, autrement dit, chaque individu touche la même part de création monétaire. On peut donc parler en masse monétaire :
La masse monétaire $M(t)$ dans la population $X$ vaut
$$  M(t) = \int_{X} m(x,t) \mathrm{d}x $$
La symétrie temporelle implique
$$ \frac{\mathrm{d} M}{\mathrm{d} t} = cM(t) $$
autrement dit, chaque génération créera une *proportion égale* ($c$) de la masse monétaire courante ($M$). Aucune génération ne sera avantagée par rapport aux générations suivantes, contrairement à l'or ou au bitcoin (où les premiers arrivés étaient avantagés). 
Les solutions à cette équation sont de la forme :
$$ M(t) = M(0)e^{ct} $$

#### Dividende universel
Pour une zone économique constituée de N individus, le dividende universel (DU) vaut :
$$\mathrm{d}m = \frac{\mathrm{d}M}{N}$$
or
$$dM = cM\mathrm{d}t$$
donc

\begin{equation}\label{du} \text{DU} = c\frac{M}{N}\mathrm{d}t \end{equation}

#### Choix de c
Si l'on choisit $c$ trop grand, la monnaie va se "dévaluer" trop vite, ce qui empêche sa fonction de réserve de valeur à court terme (similaire à une hyper-inflation) et avantagera les individus lorsqu'ils sont jeunes. Si, au contraire, c est choisi trop petit, la monnaie manquera et avantagera les individus lors de leur vieillesse. La TRM propose une valeur pour $c$ calée sur l'espérance de vie $\text{ev}$ de l'individu.

Il paraît naturel d'adapter le rythme de création monétaire à l'espérance de vie. Je propose une valeur de

\begin{equation}\label{c} c = 2ln(2)/\text{ev} \end{equation}

pour certaines raisons détaillée par la suite.

### Corolaires

[//]: # "Les conséquences d'une monnaie telle que proposée par la TRM sont nombreuses. J'en cite ici quelques unes."

#### Convergence vers la moyenne
Quand un individu entre dans la zone économique (à sa naissance), il commence à créer de la monnaie. Notons $P(t_a)$ le pouvoir de la somme monnaie créée entre sa naissance $t_0 = 0$ (prise comme référence) et son âge $t_a$. Par définition du DU (création de monnaie d'un individu), on a :
$$P(t_a) = \int_0^{t_a} \text{DU}(t)$$
soit, en remplaçant le DU par sa valeur exprimée en ($\ref{du}$)
$$P(t_a) = \int_0^{t_a} c\frac{M(t)}{N}\mathrm{d}t$$
ce qui après calcul (intégrale d'une exponentielle) donne

\begin{equation}\label{pouvoir} P(t_a) = \frac{M(t_a)}{N}(1-e^{-ct_a})\end{equation}

On peut vérifier qu'à la naissance aucune monnaie n'a été créée (on a bien $P(0)=0$). Pour un temps infini, P tend asymptotiquement vers une proportion $1/N$ de la masse monétaire, définie comme le pouvoir moyen.

<div id="graphique">
  <div><canvas id="canvas"></canvas></div>
  <div><blockquote>
    <span>Ajuster la création monétaire annuelle :</span>
    <input id="rangeInput" type="range" value="5" step="0.1" min="0" max="20" oninput="amount.value=rangeInput.value" />
    <input id="amount" type="number" value="5" step="0.1" oninput="rangeInput.value=amount.value" />
    <span id="percentage_per_year">5</span><span>% par an</span>
  </blockquote></div>
</div>

<style>
    canvas {
        -moz-user-select: none;
        -webkit-user-select: none;
        -ms-user-select: none;
    }

    #rangeInput {
        background-color: #FFF;
    }
    
    #graphique {
    	display: block;
        width:100%;
    }
    #graphique > div {
    }
</style>

<script src="https://files.trentesaux.fr/extra/courbes_interactives/js/Chart.bundle.js"></script>
<script src="https://files.trentesaux.fr/extra/courbes_interactives/js/utils.js"></script>
<script src="https://files.trentesaux.fr/extra/courbes_interactives/js/graphique.js"></script>
#### Réduction progressive des inégalités
En ajoutant la même part à tout le monde, on réduit les inégalités. Par exemple, si dans un groupe de dix personnes, une personne a réussi à prendre toute la monnaie (100% pour lui 0% pour chaque autre), si l'on donne 10% à chacun, la répartition sera moins inégale (55% pour lui, 5% pour chaque autre), si l'on donne à nouveau 10% à chacun, on réduira encore les inégalités (40% pour lui, 6.6% pour chaque autre)...
Au bout d'un temps infini (s'il n'y a pas d'échanges) tout le monde se retrouvera à 10%. La convergence vers la moyenne est vraie quelle que soit la quantité de monnaie collectée par un individu.

#### Disparition de l'impact monétaire
Quand un individu sort de la zone économique (à son décès) il cesse de créer de la monnaie. Même s'il lui reste une quantité importante de monnaie "bloquée", cette part relative décroît avec le temps. En effet par définition un pouvoir $P_d$ vaut initialement $M_d/M(0)$. Après un temps $t_d$, le pouvoir vaut $P(t_d)=M_d/M(t_d)$ soit
$$P(t_d) = e^{-ct_d}$$
Même si la quantité absolue $M_d$ de monnaie bloquée ne varie pas, son pouvoir, égal à sa quantité relative, diminue exponentiellement. C'est aussi la raison pour laquelle il faut éviter de prendre une valeur de $c$ trop grande.

#### Conséquence du choix de c
La valeur de $c$ que je propose (\ref{c}) permet d'atteindre la moitié du pouvoir moyen après la moitié d'une vie. On peut le voir en remplaçant $t_a$ par $\text{ev}/2$ dans la formule ($\ref{pouvoir}$). Cela donne lieu à un taux de croissance de la masse monétaire de 4.7%/an.

## Une monnaie démocratique
Une monnaie comme définie précédemment est intrinsèquement démocratique : la création monétaire ne dépend pas de critères arbitraires choisis par des organismes privés ou une caste politique bureaucratique. Ainsi la création monétaire de l'individu façonnera le monde dans lequel il vit. Si une majorité de citoyens décide de délaisser une agriculture industrielle polluante et destructrice au profit d'une agriculture qui respecte l'environnement, le paysage économique sera modifié sans attendre la bonne volonté d'un homme politique ou les investissements des marchés financées. Si nous voulons faire progresser la démocratie il nous faut choisir une monnaie de manière démocratique.

### Rapprochement avec le revenu de base
Un mouvement international précède l'apparition de la monnaie démocratique : les partisans d'un "revenu de base inconditionnel", "revenu universel", "universal basic income", mené en France pas le [MFRB](https://www.revenudebase.info/). La monnaie démocratique formalise cette idée et la place au centre de la création monétaire. Le site [creationmonetaire.info](https://www.creationmonetaire.info/) donne l'ordre de grandeur d'un dividende universel de 674 € / mois / citoyen en France pour le niveau de création monétaire actuel.

### Financer ce qui n'est "pas rentable"

Aujourd'hui les financements se font uniquement sur un critère de rentabilité. Aussi bien le financement des entreprises que des États. Quand un secteur ou un État n'est plus jugé rentable pour les investisseurs, il ne reçoit plus de financement et fait faillite, comme ça a été le cas pour la Grèce. Les dépenses publiques n'étant pas "rentables" elles tendent à disparaître ou à se privatiser.
En prélevant seulement 30% du DU de ses citoyens et en supprimant toute forme de taxe et d'impôt, la France pourrait couvrir les dépenses publiques pour l'éducation, la santé, les transports et la culture bien mieux qu'elle ne l'a jamais fait. Cette affirmation appelle à démonstration et fera, je l'espère, l'objet d'un prochain article. 




[définitions]: http://trm.creationmonetaire.info/definitions.html
[solutions]: http://trm.creationmonetaire.info/solutions.html