Title: Présentation en reveal + hugo
Date: 2018-06-23
Authors: Hugo Trentesaux
Summary: Comment créer un présentation en reveal.js avec le générateur de site statique en go hugo.

Il est très simple de créer une présentation en [reveal.js](https://revealjs.com/#/) grâce au générateur de site statique hugo [gohugo.io](https://gohugo.io/) et au template reveal.js associé [reveal-hugo](https://themes.gohugo.io/theme/reveal-hugo/#/).

Installer hugo, créer un nouveau site, et ajouter le thème.

    sudo apt install hugo
    hugo new site exemple
    cd exemple/
    git init
    git submodule add https://github.com/dzello/reveal-hugo.git themes/reveal-hugo

ajouter dans config.toml

    theme = "reveal-hugo"

    [outputFormats.Reveal]
    baseName = "index"
    mediaType = "text/html"
    isHTML = true

créer ensuite dans le dossier 'content' un fichier \_index.md avec le contenu

    +++
    title = "exemple"
    outputs = ["Reveal"]
    +++

    # titre

    Contenu 1

    ---

    ## titre 2

    contenu 2

Pour les afficher, lancer

    hugo server

et se rendre dans le navigateur à l'adresse [localhost:1313](http://localhost:1313).