Title: Internet est-il gratuit ?
Date: 2017-05-11
Tags: internet, google, facebook
Slug: le-vrai-cout-d-internet
Summary: L'informatique est quelque chose de magique pour toi ? Tu ne comprends pas comment des outils comme Google et Facebook peuvent être gratuits ? Tu ne t'es même pas posé la question ? J'ai écrit cet article pour toi, j'espère qu'il t'aidera !
Status: draft

# Internet est-il gratuit ?

L'informatique est quelque chose de magique pour toi ? Tu ne comprends pas comment des outils comme Google et Facebook peuvent être gratuits ? Tu ne t'es même pas posé la question ? J'ai écrit cet article pour toi, j'espère qu'il t'aidera !
Je ne parle pas du tout du coût des infrastructures ni des services liés à l'utilisation d'internet, pas plus que de l'immense travail librement accessible via le logiciel libre.

## La publicité en ligne

Une réponse simple est : une grande partie des sites web sont financés par la publicité. Des entreprises qui veulent faire de la publicité pour leur produit achètent de l'espace publicitaire sur des sites et payent en fonction du nombre de personnes ayant vu la publicité, cliqué dessus, ou quelque chose comme ça. Mais cette réponse n'est ni complète ni satisfaisante. À qui profite cette publicité ? Quels mécanismes sont mis en jeu ?

### AdWords, AdSense, et publications sponsorisés

Sauriez vous citer des entreprises de publicité en ligne ? Vous connaissez bien leurs noms : les plus grands sont Google et Facebook, ils concentrent une écrasante majorité des revenus publicitaires en ligne. Le chiffre d'affaire annuel de Google est de 90 Milliards de dollars, dont 92% provient de la publicité. Facebook totalise 10 Milliards de dollars de chiffre d'affaire. C'est un commerce juteux, car plus d'un tiers de l'investissement publicitaire mondial concerne la publicité en ligne (soit presque 1% du PIB mondial).

Fini les chiffres, voyons les formes que prend cette publicité. Le produit principal de Google est AdWords. Ad pour "publicité", Words pour "mots". Google vend des mots aux enchères. Lorsqu'un utilisateur rentre ces mots dans le moteur de recherche, il trouve en premier résultat ce qu'a voulu celui qui a remporté l'enchère. C'est aussi simple que ça, et c'est pourtant l'essentiel des revenus de Google.

Ensuite seulement, viennent les publicités que vous voyez sur les bords des sites, dans un petit encart. Il y a marqué AdSense en général en dessous. AdSense, c'est le deuxième produit de Google. C'est une régie publicitaire, c'est-à-dire une entreprise qui met en lien celui qui veut diffuser sa publicité, et celui qui propose un espace publicitaire. Google agit ici comme un intermédiaire : il s'occupe de distribuer la publicité, et prélève une commission pour la vente. C'est également le programme utilisé dans Youtube, mais cette fois-ci, Google propose lui même la plateforme pour que les utilisateurs puissent diffuser leur contenu. 

Enfin, il y a la publicité "à la Facebook". Ici, la plateforme est à la fois la régie publicitaire et l'espace publicitaire. Il est possible de "promouvoir" une de ses publications sur Facebook en payant une certaine somme. Plus cette somme sera élevée, plus Facebook montrera cette publication à un grand nombre d'utilisateurs du réseau social.

Il existe d'autres formes de publicité, par exemple le contenu sponsorisé, mais ça commence à faire beaucoup pour cet article, alors je le réserve pour un autre.

### Le coût de cette publicité

À chaque forme de publicité son impact, nous allons parler des dangers que ces publicités représentent pour nous, à coup de détournement de l'économie, dépendance des médias, et un phénomène d'*echo chamber* publicitaire.

La machinerie AdWord n'a pas fait le succès que de Google. Elle a aussi permis à des entreprises comme Expedia de profiter du système. Je vais prendre l'exemple de l'hôtellerie car il est très parlant, mais c'est également le cas pour la restauration, les comparateurs de prix, les agences de voyage, bref, tout ce qui concerne de près ou de loin les achats en ligne. Pour l'hôtellerie, deux géants Américains se partagent le marché avec encore une fois un monopole écrasant : Expedia et Priceline avec des chiffres d'affaires respectifs de 8 et 3 milliards de dollars. Ils achètent sur AdWords les noms de tous les hôtels et toutes les combinaisons de mots comme par exemple "hôtel + Lyon" ou "hôtel + pas cher + Londres" pour que l'utilisateur de Google tombe en premier choix sur leurs multiples sites (parmi lesquels booking.com, par exemple). Il prennent aux hôtels une commission énorme sur les chambres qu'ils vendent en leur nom : autour de 30%. Je vous passe les méthodes honteuses *et illégales* utilisées pour asservir les commerces et vous livre tout de suite le bilan : les commerces sont affaiblis par ces commissions énormes, les entreprises font de l'optimisation fiscale sur leur recettes pour ne pas payer d'impôts, et Google touche des sommes énormes sans salir son image, et en payant moins de 1% des impôts qu'il devrait payer en France.

Un autre secteur est violemment touché par la publicité, ce sont les médias. Avec l'arrivée d'internet, les journaux ont vu une majorité de leur audience déplacée en ligne. La plupart ont rendu leur contenu gratuit et se financent uniquement par la publicité. Hors, si les annonceurs n'ont pour l'instant pas le choix des sites et contenus devant lesquels sont montrés leurs publicités, Google se réserve le droit de fixer ces règles. Ainsi, les contenus présentant de la nudité sont totalement bannis par Google. Ils ne peuvent pas recevoir de revenus publicitaires via AdSense. Plus grave, les annonceurs font pression sur Google pour qu'il censure aussi les sites gays, les "contenus polémiques" et les "sujets sensibles", bref, tous les contenus qui ne sont pas "advertiser friendly". Ainsi, des centaines de Youtubeurs ont vu leur contenu privé de monétisation sous prétexte qu'ils abordaient des sujet comme la guerre du Golfe, qu'ils utilisaient des mots grossiers ou qu'ils abordaient des sujet controversés. Cela les pousse à l'autocensure. Quand on sait que HSBC a failli couler *The Guardian* en retirant ses investissements publicitaires du journal, on n'a pas envie de laisser les médias aux mains d'une machine à l'éthique aussi douteuse.

Un des effets de cette omniprésence publicitaire est que l'on finit vite par oublier qu'il existe des alternatives, qui n'ont souvent pas les moyens de payer des campagnes publicitaires géantes. L'utilisateur d'internet est enfermé sans s'en rendre compte dans une chambre à écho du consommateur, celle-là même qui favorise la montée des comportements "extrémistes", qui a fait voter le Brexit... et qui maintient la suprématie des plus grandes entreprises [(ref)][2].

### Des solutions

Il existe heureusement des solutions à ces problèmes. Je vous en propose quelques unes.

Une extension utile sur votre navigateur web est un bloqueur de publicité. Il permet de bloquer pratiquement toute forme de publicité. Là encore, il existe des arnaques, car certains bloqueurs de publicité acceptent de l'argent pour mettre un site sur liste blanche, et donc cesser de bloquer la publicité qu'il affiche. Je vous suggère donc Ublock Origin, qui n'accepte aucune forme de don, pour rester à l'abri de toute corruption. Le problème du bloqueur de publicité est qu'il coupe aussi les financements de sites honnêtes qui ont besoin de publicité pour fonctionner. Il vous appartient donc de choisir quels sites vous voulez soutenir en désactivant spécifiquement votre bloqueur de publicité. Ublock est pratique pour cela, car il affiche un gros bouton permettant de l'activer ou désactiver à la demande pour un site spécifique.

Il existe heureusement d'autres sources de financement que la publicité comme par exemple le financement participatif ou le modèle économique payant. Il existe de multiples plateformes de dons singuliers ou récurrents sur lesquels vous pouvez soutenir financièrement des associations et particuliers dont le travail vous plaît.

Vous pouvez également choisir des plateformes plus respectueuses de l'utilisateur, ce qui m'amène à ma deuxième partie : les données personnelles.

## Les données personnelles

La guerre de la publicité, comme toute guerre, a fait des dommages collatéraux dont les citoyens sont les victimes. Pour augmenter l'efficacité des publicités et fidéliser les annonceurs, les géants de la publicité ont profité d'un terrain encore non régulé pour développer une machinerie de surveillance digne de 1984. Bienvenue dans l'ère du Big Data.

### La collecte de données

Pour commencer, nous allons voir où les entreprises d'internet prélèvent leur données, et ce qu'elles peuvent en déduire.

La première action que tout le monde a fait sur internet est certainement une requête sur le moteur de recherche Google, beaucoup de gens en font des dizaines par jour. Google développe des technologies poussées pour suivre à la trace et analyser ces requêtes de telle sorte qu'il est capable de faire une liste de *toutes* les recherches Google que chacun fait. Si vous facilitez le travail de Google en vous connectant à un compte Google, vous avez même le droit d'accéder à certaines de ces données ici [(https://myactivity.google.com)](https://myactivity.google.com), ce qui vous permettra de mieux vous rendre compte de ce que Google sait sur vous. Grâce aux milliards de recherche faites chaque jour, Google est capable d'analyser ces recherches avec un finesse inimaginable, dont je parlerai peut être dans un autre article. Google ne se contente pas de ça et lit tous vos emails et ceux de vos correspondants sur votre adresse Gmail, tous vos documents sur le Google drive, toute votre activité sur le navigateur Chrome, et ainsi de suite. Il brasse très large.

Facebook a aussi accès à des données personnelles et s'est spécialisé dans la constitution d'un réseau de connaissances à haut degré de précision. Ainsi, il connaît précisément les connaissances de plus de 31 millions de Français inscrits sur Facebook, mais aussi de leurs amis non inscrits grâce à la collecte des carnets d'adresse. En effet, si vous autorisez une seule des applications mobiles Facebook, Messenger, Instagram, ou Whatsapp à accéder à vos contacts, c'est tout votre carnet d'adresse qui est siphonné. On sait que Facebook associe à chaque humain un profil fantôme lié à son numéro de téléphone afin de rassembler des informations sur tout le monde, y compris les personnes non inscrites. Facebook scanne tous les posts, tous les messages, toutes les images, toutes les activités de ses utilisateurs pour établir un profil bien plus riche que tout ce qu'on a l'impression de rendre public sur Facebook. Facebook a des algorithmes de reconnaissance faciale très performants, notamment grâce à l'acquisition de Instagram pour un milliard de dollars, si bien qu'il est capable de deviner qui est présent sur une photo même si on ne le lui précise pas. Même si Facebook n'a pas accès au contenu de vos messages sur Whatsapp, il a racheté cette entreprise pour 20 milliards de dollars pour avoir accès aux métadonnées des conversations, c'est à dire l'expéditeur, le destinataire, l'heure  d'un message, la durée d'un appel, la taille d'une image... Des informations qui en disent plus sur le message que son contenu, grâce aux algorithmes dits de "Big Data".

Google, Facebook et Twitter ont même trouvé un moyen d'étendre leur surveillance au delà de leur territoire via les "boutons de partage". Ces boutons, que personne n'utilise, sont présent sur beaucoup de sites, qui se donnent l'impression d'être connectés. Ces boutons sont les yeux et les oreilles de ces grandes entreprises. Ils leurs permettent de rapporter l'activité de leurs utilisateurs via des cookies même quand ils naviguent en dehors de leur site. Pour résumer, si vous ne vous protégez pas, ils savent presque tout de votre activité en ligne, hors votre activité en ligne est souvent le reflet de votre activité hors ligne.

### La publicité ciblée

Tout cet arsenal a été développé pour proposer aux annonceurs publicitaires de mieux cibler leur public. Ainsi, si on sait que vous êtes végétarien, vous serez probablement plus intéressé par une publicité pour un steak végétal que pour le nouveau jambon sans sel, lequel sera plutôt montré à quelqu'un qui se soucie de sa santé et qui a recherché plusieurs fois "régime sans sel" sur Google.
De plus, ces données amassées permettent aux marchands en ligne d'adapter leurs prix à leur public [(ref)][1].

C'est tout.

Voilà ce pour quoi nous sacrifions l'ensemble de nos données personnelles. Pour permettre à des entreprises d'optimiser l'espace publicitaire. Mais cette surveillance peut avoir des effets pervers, nous allons nous y intéresser dans un prochain article.

[1]: https://spreadprivacy.com/ads-cost-you-money/ "Duckduckgo"
[2]: https://spreadprivacy.com/filter-bubble/ "Duckduckgo"