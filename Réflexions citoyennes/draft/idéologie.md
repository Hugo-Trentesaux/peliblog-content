Title: Idéologie
Date: 2019-01-01
Authors: Hugo Trentesaux
Summary: Construction idéologique ?
Status: draft


## Dictature et idéologie

La nature humaine comporte du mal, qui mène à la violence, et du bon, qui mène à l'harmonie. Les sociétés reproduisent ces aspects de l'individu à toutes les échelles à travers des guerres et des systèmes de domination, mais aussi des structures d'entraide et de paix collective.

Je réfléchis ici au pendant des idéologies dictatoriales, dans l'idée de construire un système qui puisse s'y opposer. 

La dictature peut prendre plusieurs formes :

- les dictatures d'État (Chinoise, Nord-Coréenne, Russe, Islamiste...)
- la dictature de l'argent (capitalisme, pétrole...)

et repose sur une idéologie comme :

- le rêve Chinois (idéologie du parti communiste de Chine),
- le djihadisme (idéologie du parti islamiste) 
- le pragmatisme (idéologie dominante occidentale)

Pour résister à ces dictatures, il faut organiser une société munie d'une **idéologie saine**. La *déclaration universelle des droits de l'homme et du citoyen* offre de bons fondements idéologiques, mais ces droits ne sont ni appliqués ni correctement protégés, même dans leur pays d'origine.

Je souhaite proposer des idées pour les mettre en œuvre. 

**Art. 1er.** Les hommes naissent et demeurent libres et égaux en droits. 

Ces droits comportent l'accès à la richesse, l'éducation, l'information, la culture, la justice, les soins. La liberté comporte la liberté d'expression, la liberté à disposer de son temps.

**Art. 13.** Pour l'entretien de la force publique, et pour les dépenses d'administration, une contribution commune est indispensable : elle doit être également répartie entre tous les citoyens, en raison de leurs facultés.

Pour lutter contre les inégalités de richesse, nous avons mis en place des systèmes de redistribution (impôts, taxes, cotisations, allocations, chômage, retraite). Mais ce système est souvent perçu comme une injustice. Je propose de mettre en place un **système monétaire à dividende universel** comme défini par la théorie relative de la monnaie. Chaque individu doit participer d'une part égale de sa création monétaire aux dépenses collectives permettant de garantir l'égalité citée ci-dessus.