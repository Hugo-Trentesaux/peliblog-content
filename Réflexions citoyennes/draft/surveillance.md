Title: Les dangers de la surveillance
Date: 2017-05
Tags: surveillance
Slug: les-dangers-de-la-surveillance
Summary: La surveillance sur internet restructure la société. Je parle ici des applications existantes et futures de cette surveillance.
Status: draft

# Les dangers de la surveillance

Cet article fait suite au [précédent]{internet-gratuit}, qui parle de la mise en place de la surveillance généralisée via l'outil publicitaire. Ici, je vais parler de la façon dont ces technologies de surveillance sont utilisées dans le présent et pourront être utilisées dans le futur. 

## "L'avant"

Par "l'avant", expression que j'emprunte à XXX, je désigne ce pour quoi la surveillance est déjà utilisée. Nous avons déjà abordé le sujet de la publicité ciblée dans l'article précédent, il faut juste retenir que cette publicité ciblée ne se justifie pas d'elle même, qu'elle a permis de développer un outil de surveillance, et qu'elle ne fait qu'asseoir la position des entreprises déjà dominantes sur le marché. Nous allons parler ici d'autres applications de cet outil de surveillance, dont certaines sont particulièrement pernicieuses, je vais essayer de montrer pourquoi.

### Les compagnies d'assurances

Les compagnies d'assurance sont parmi les premières à s'être emparées des outils de profilage. Ainsi, certaines compagnies d'assurances proposent des réduction à leur clients contre certaines données. Par exemple, xxx propose une réduction de 5% à 20% si on lui donne accès à notre compte Facebook. Il est en effet montré que l'on peut deviner les habitudes alimentaires, la consommation de drogues, l'état de santé mentale et physique, le goût du risque, et plein d'autres facteurs qui permettent aux compagnies d'assurances d'estimer le risque, et donc de sécuriser leurs profits.

Dans ce cas de figure, impossible de parler de "gagnant-gagnant". En effet, même si l'assurance optimise ses profits et vous déduit d'une partie des frais, ce n'est pas le cas pour tout le monde. Les réductions dépendent de ce qui est trouvé dans vos données personnelles, certaines personnes peuvent ne pas y être éligibles, il faut accepter de céder ces données pour avoir une chance d'obtenir une réduction, la compagnie d'assurance peut refuser de vous assurer sans motif explicite. Bref, ce que l'on essaye de faire ressembler à une utopie est en effet une dystopie. En proposant de telles "offres", les compagnies d'assurances préparent un monde encore plus inéquitable, contre le principe de mutualisation des risques, dans lequel les fumeurs, les personnes buvant régulièrement, et les personnes ne pratiquant pas d'activité sportive seront rejetées de la société.

banques

### Les tirs de drones

L'armée américaine bénéficie du programme secret de surveillance le plus sophistiqué et le plus complet. En effet, le budget annuel des services de renseignement représente xxx milliards de dollars, soit xxx % du chiffre de la défense. Les informations collectées sur les citoyens américains sont théoriquement limitées par la Bill Of Rights (?) mais à cause du Patriot Act, que Donald Trump essaye d'intégrer à la constitution (?) les données de plusieurs millions de citoyens américains sont exploitées sans limitation. Quand il s'agit de citoyens étrangers, il n'y a aucune limitation. L'armée américaine se permet de déclencher des tirs de drone simplement d'après les renseignements accumulés sur une personne. Le nombre de personnes ainsi exécutées s'élève à plus de xxx, sans compter les "dommages collatéraux", c'est-à-dire les civils assassinés involontairement lors des frappes aériennes. Je ne peux pas concevoir que l'on laisse une arme telle que la surveillance entre les mains de pareils bourreaux.


## "L'après"






la grande notation (uber, )