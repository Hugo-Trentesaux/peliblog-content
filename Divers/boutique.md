Title: Boutique
Date: 2017-12-31
Authors: Hugo Trentesaux
Summary: Short version for index and feeds
Status: draft

# Boutique

Une idée qui me trotte dans la tête depuis longtemps est celle d'ouvrir un boutique de goodies scientifiques. Les youtubeurs vendent des T-shirts, pourquoi pas moi ? Chaque objet doit contenir une brève description 

- bol / vase / tasse de forme géométrique (paraboloïde de révolution, hyperbole)
- bouteille de klein en verre
- T-shirt à qr code qui mène vers une page wikipedia intéressante, changeable depuis une application smartphone
- 
- 