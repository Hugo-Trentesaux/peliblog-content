Title: Économie
Date: 2018-10-28
Authors: Hugo Trentesaux
Summary: Quelques références que j'ai aimé sur l'économie.
Status: draft

# Films
## The big short

![the big short](https://upload.wikimedia.org/wikipedia/commons/4/45/The_Big_Short_Film_Logo.png)

The Big Short parle de la crise des subrimes à travers les points de vues de plusieurs personnages qui l'ont vu venir.

## Inside Job

![inside jobs](https://upload.wikimedia.org/wikipedia/en/thumb/a/a1/InsideJob2010Poster.jpg/220px-InsideJob2010Poster.jpg)

Inside Job est un documentaire sur la crise de subrimes. Les peronnes interviewées ont eu un rôle clé dans cette crise.

# Livres
## The Production of Money

Ann Pettifor est une analyste financière britannique. Son livre apporte d'excellentes pistes de réflexion au sujet de la monnaie.

![The production of Money](https://assets.vogue.com/photos/58b8684661606a75f4402947/master/pass/01-embed-money.jpg)
