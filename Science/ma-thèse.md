Title: Ma thèse
Date: 2018-05-19
Authors: Hugo Trentesaux
Summary: Un résumé de ma thèse sans prérequis scientifiques !

Je consacre en ce moment trois années de ma vie à mon travail de thèse : améliorer la compréhension du cerveau, en particulier de l'intégration des entrées sensorielles. Il me semble important de rendre le sujet de mon travail accessible sans trop de prérequis scientifiques, c'est pourquoi j'ai écrit l'article qui suit.

> L'intégration multisensorielle à l'échelle du neurone dans le cerveau de poisson zèbre

## Le poisson zèbre, un animal modèle

Quand j'évoque le titre de ma thèse, la première réaction est souvent la surprise de l'animal choisi pour l'étude. Il existe des animaux de laboratoires connus du grand public comme la souris ou la mouche (drosophile), mais le poisson zèbre est également très utilisé. Un "animal modèle" en biologie est une espèce particulière sur laquelle les scientifiques s'accordent pour travailler. Cela permet de profiter plus simplement des différents travaux. Par exemple, il existe plusieurs lignées mutantes très connues et très répandues dans les laboratoires qui en facilitent l'étude.
Le poisson zèbre a l'avantage d'être facile à élever en aquarium (on en trouve souvent dans les aquariums publics), d'avoir un rythme de reproduction très élevé, et d'être quasiment transparent à l'état d'œuf et de larve. Cela a permis d'étudier les phases de développement précoce.

![larva]({static}/images/zebrafish_larva.jpg)
> Une larve de poisson zèbre vue de dessus. Le tuyau près de sa tête mesure 1.5 mm. On voit entre autres ses yeux noirs et sa vessie natatoire.

## Des mutants fluorescents

La GFP (green fluorescent protein) est une protéine fluorescente découverte dans la nature dans des méduses. Des chimistes, physiciens et biologistes ont compris comment la modifier pour nos besoins et intégrer son ADN dans des cellules de poissons zèbre. Les poissons que nous utilisons  au laboratoire, des lignées mutantes appelées GCamp6, sont le résultat d'un long travail de biologie et de génie génétique. En effet, ils produisent une version modifiée de la protéine fluorescente dans chacun de leurs neurones (et uniquement de leurs neurones !). Cette version modifiée est fluorescente uniquement en présence d'ions Calcium, or, quand un neurone est actif, la concentration d'ions Calcium à l'intérieur augmente. Ainsi, chaque neurone actif (et seulement ceux actifs) devient fluorescent dans le cerveau du poisson, ce qui nous permet de l'imager. On appelle ça "l'imagerie calcique".

![fluo]({static}/images/fluo_cerveau.jpg)
> Le poisson a une fluorescence naturelle jaune, la GFP s'exprime dans son cerveau en vert

## Le microscope à feuille de lumière

Attention, un objet fluorescent n'émet pas lui-même de lumière (les objets *phosphorescents* en émettent) ! Il se contente d'absorber la lumière et de la *re-émettre* dans une autre couleur. Pour observer la fluorescence du neurone, il faut donc l'éclairer. Nous utilisons pour cela un laser bleu, et la fluorescence apparaît verte. Si nous éclairions tout le cerveau d'un coup, nous ne serions pas capables de discerner toutes les couches. Mon laboratoire a donc mis au point une technique d'éclairage appelée la "feuille de lumière" (ou "nappe laser"). Cela consiste à envoyer un faisceau plat de lumière et à scanner le cerveau couche par couche pour obtenir une image en trois dimensions. Nous pouvons ainsi observer presque tous les neurones du poisson en temps réel.

[//]: # "TODO insert this in template"

<div style="width: 80%; padding-top: 45%; position: relative;">
<video width="100%" height="100%" style="position: absolute; top: 0; left: 0; bottom: 0; right: 0;" src="https://files.trentesaux.fr/extra/blog/zf_brain.webm" type="video/wemb" controls autoplay loop></video>
</div>

> Film d'une couche du cerveau de poisson zèbre. Lignée cytoplasmique (marquage dans le cytoplasme)

<div style="width: 80%; padding-top: 45%; position: relative;">
<video width="100%" height="100%" style="position: absolute; top: 0; left: 0; bottom: 0; right: 0;" src="https://files.trentesaux.fr/extra/blog/brain_scan.webm" type="video/wemb" controls autoplay loop></video>
</div>

> Couches successives allant du haut du cerveau vers le bas par pas de 0.5 µm. Lignée nucléaire (marquage dans le noyau)

## L'intégration multisensorielle

Ces mots ne devraient pas faire peur, ils désignent quelque chose que nous faisons quotidiennement des milliers de fois ! Cela consiste tout simplement à combiner plusieurs sens comme la vue, l'audition, l'odorat pour déclencher une action ou une perception. Par exemple voir de la fumée et sentir l'odeur du brûlé, entendre le klaxon et voir la voiture, ou même sentir le vent sur sa peau et l'odeur de la mer. Certains neurones s'occupent de la vision, d'autres de l'odorat, nous cherchons des neurones qui intègrent plusieurs modalités sensorielles et améliorent la perception d'un événement. La route est longue, mais petit à petit, la compréhension du fonctionnement du cerveau s'améliore et la science progresse dans l'infinité de choses qu'il nous reste à découvrir.

[//]:# "les publis scientifiques"