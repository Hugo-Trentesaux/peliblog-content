Title: Séparation des pouvoirs
Date: 2019-02-16
Authors: Hugo Trentesaux
Summary: Montesquieu parlait de séparation des pouvoirs : judiciaire, exécutif et législatif. Aujourd'hui les nouveaux pouvoirs sont médiatique, financier et technologique. Il est temps de penser à les séparer...
Status: draft

La monarchie regroupait les pouvoirs législatif, exécutif et judiciaire. Leur séparation a permis à la république d'exister. Si cette séparation est toujours nécessaire aujourd'hui, elle n'est pas suffisante pour un fonctionnement démocratique. Le XXIème siècle a en effet vu trois nouveaux pouvoirs émerger et fusionner : les pouvoirs médiatique, financier et technologique. Je pense que ces pouvoirs devraient être séparés et expose ici quelle serait mon approche.

- intro : reformuler
  - défintion pouvoirs
  - comment sont-ils liés
- 

**Le pouvoir financier** est certainement celui le mieux perçu par le peuple : des entreprises gigantesques au budget comparables à celui des états s'organisent en lobbys pour influencer les lois et décisions politiques et s'arment d'avocats pour s'affranchir de la justice.

[budget de Bayer/GAFAM vs budget d'états, d'institutions européennes]
[budget des lobbys]
[budget en avocats]

Cela se ressent dans l'organisation des villes où les banques occupent des places de choix ; dans la vie politique, où des grandes fortunes ou des anciens des banques prennent le pouvoir (Trump, Macron) ; ou même lors de procès inégaux où le faible ne peut pas résister à l'armée d'avocats du fort (agriculteur vs Monsanto, Denis Robert vs Clearstream).

**Le pouvoir médiatique** permet d'informer le peuple, et donc de manipuler l'opinion publique. À travers les médias traditionnels (journaux, radio, TV) aussi bien que nouveaux (fils d'actualité, YouTube, ) le pouvoir politique se maintient en place, les grandes fortunes évitent le message qui les dérange. 

Les principaux médias sont sous le contrôle d'une élite restreinte et les autre sont achetés par la publicité, poussés à l'autocensure, réduits au silence par des conditions économiques impossibles à remplir, tués par une simili information diffusé gratuitement.

[pieuvre médiatique]

**Le pouvoir technologique** s'ajoute aux précédents. L'informatique, outil magique qui permet le partage de la connaissance à l'échelle de l'humanité se transforme en magie noire dans les mains du pouvoir centralisé. Outil ultime de surveillance, d'asservissement des cerveaux, contrôle total de la consommation, il annihile la liberté de l'individu en l'emprisonnant dans un espace virtuel sur mesure. Sous couvert de transhumanisme, d'intelligence artificielle, de facilitateur universel, la technologie est en mesure de prendre le pas sur l'homme et de transformer le pouvoir en une mécanique bien huilée à l'écart de toute décision humaine... mais contrôlée par une poignée d'individus privilégiés.

Les GAFAM en tête, il s'empare de toute activité économique via des plateformes qui abusent de leur pouvoir : AirB&B, Uber, Booking.com, Deliveroo drainent l'économie pour le bénéfice du petit nombre placé loin devant l'intérêt général.

**pouvoirs liés**

Ces pouvoirs sont intriqués, et contrôlent ensemble l'évolution de notre monde dans la plus grande indifférence de la volonté démocratique. Pour construire un monde capable de s'attaquer au réchauffement climatique, de tendre vers une justice et une paix sociale, et de respecter les droits de l'homme, il me semble indispensable de séparer ces trois pouvoirs. Pour cela, nous avons besoin d'un soulèvement massif du peuple et d'une réflexion autour de ces sujets.



**s'émanciper du pouvoir financier**







**Une monnaie libre**



**Le logiciel libre**



**Une presse libre**

