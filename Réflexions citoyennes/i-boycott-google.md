Title: I-boycott Google
Date: 2017-04
Tags: surveillance, Google, i-boycott
Slug: i-boycott-google
Summary: J'ai créé une campagne de boycott contre Google, celle-ci n'a pas passé la phase de validation.

# I-boycott Google

## Le site i-boycott

Le site i-boycott est une plateforme de boycott citoyen. Il permet à n'importe qui de lancer une campagne de boycott contre un produit ou une entreprise. Consultez le [site officiel](https://www.i-boycott.org) pour en comprendre le fonctionnement. J'ai essayé de lancer ma propre campagne, mais elle n'a pas porté ses fruits. Voici mon texte ci-dessous.

## La campagne contre Google

### Titre
Google : pour le respect de la vie privée et contre l'optimisation fiscale

### Texte

Google a toujours été un immense moteur de progrès. Il a permis le développement de technologies performantes qui nous sont utiles tous les jours. Cependant, bien qu'il soit facile de l’ignorer, en utilisant les outils de Google gratuitement, nous les payons autrement : avec nos **données personnelles**. Ainsi, chaque requête sur Google est associée à un utilisateur, via son adresse IP, des cookies, un ensemble d'informations sur son navigateur, son système d'exploitation, la taille de son écran, le modèle de son appareil. En combinant ces données et la grande quantité de recherches faites chaque jour par **93% des français**, il est possible de deviner l'âge, le genre, les habitudes, la classe sociale, le métier de l'utilisateur. Il est même possible de prédire s'il est malade, si elle est enceinte, s'il fume, s'il a réussi ses examens, pour qui il va voter !

Google se sert de cette collecte gratuite de données pour gonfler son immense chiffre d’affaires : **90 Milliards de $** en 2016, dont 92 % provient de la publicité ([webrankinfo](http://www.webrankinfo.com/dossiers/google/chiffres-cles)). En effet, il exploite ces données personnelles pour cibler l’audience et améliorer l’efficacité des publicités. Cela attire les entreprises qui achètent la publicité, qui sont donc clients de Google, faisant de nous ses produits ([la quadrature du net](http://www.laquadrature.net/fr/si-vous-etes-le-produit)).

Les dangers de cette collecte de données sont très bien expliqués par l’association Framasoft, sur sa page [dégooglisons internet](https://degooglisons-internet.org/). J’en cite ici les principaux :  

- Surveillance de l'État sur ses citoyens, repérage de l'opposition politique
- Intrusion dans les affaires privées, notion même de "vie privée" compromise
- Centralisation, risque de censure ([article du framablog](https://framablog.org/2016/10/31/si-google-vous-ignore-votre-projet-est-en-peril/))

Ces pratiques entraînent l'auto-censure et fournissent des outils de manipulation aux gouvernements et aux grandes entreprises ([Amnesty sur la surveillance de masse (en)](https://www.amnesty.org.uk/issues/Mass-surveillance)). Conclusion : les pratiques de Google en matière de respect de la vie privée sont contraires aux droits de l’homme, à la liberté d’expression et sont donc antidémocratiques.

De plus, Google détourne l'économie de notre pays. En effet, en proposant de lier des annonces publicitaires à des mots clés via Adwords, Google facilite l'insertion d'intermédiaires entre le consommateur et les commerçants. Ces intermédiaires prélèvent souvent des commissions énormes (parfois jusqu'à 50%) et rémunèrent Google indirectement via la publicité. L'hôtellerie et la restauration sont particulièrement touchées, mais c'est également le cas de nombreux services qui ont vu leurs dépenses publicitaires augmenter, et donc leur capacité à embaucher diminuer.

D’autre part, sur les bénéfices ainsi réalisés, Google pratique une **optimisation fiscale massive** pour laquelle la société a été attaquée plusieurs fois par l'Union Européenne ([article du journal Le Monde](http://www.lemonde.fr/economie/article/2014/06/11/optimisation-fiscale-l-ue-s-appreterait-a-lancer-une-enquete-sur-3-pays_4435744_3234.html)). L'entreprise a déjà été l'objet d'un redressement fiscal de **1 milliard d'euros en 2014** ([article du journal le Figaro](http://www.lefigaro.fr/societes/2014/02/04/20005-20140204ARTFIG00426-google-redresse-d-un-milliard-d-euros-par-le-fisc.php) ou encore un [autre article du Figaro](http://www.lefigaro.fr/vox/economie/2015/08/17/31007-20150817ARTFIG00227-evasion-fiscale-comment-google-et-les-transnationales-jouent-avec-les-frontieres.php)) mais rien n'indique qu'elle ait pris des mesures adéquates. Cette vidéo explique le montage financier utilisé pour échapper à toute taxation.

C'est principalement pour ces raisons que nous appelons au boycott du **moteur de recherche Google**. Nous ciblons particulièrement le moteur de recherche, car il est très facile de trouver des alternatives. Je vous suggère d'aller voir dans **l'onglet "alternatives"** les solutions proposées par la communauté !
L'objet de notre réclamation auprès de Google est que le développement de nouvelles technologies ne se fasse pas au détriment des droits de l'homme, ni de l'économie locale. Nous souhaitons obtenir **plus de transparence sur la collecte des données** (les CGU ne suffisent pas), accompagnée d'une vraie **démarche d'information de l'utilisateur** sur ce que deviennent ses données. Nous demandons une **meilleure prise en compte des réclamations de la CNIL**, ainsi qu'une **situation fiscale irréprochable**. 

### Liste de sources

- [Framasoft : dégooglisons internet](https://degooglisons-internet.org/)
- [l'évasion fiscale selon Google](https://youtu.be/mqgZ5immbMA)
- [Envoyé spécial : la face cachée de Google](https://youtu.be/hrEihFkjgwg)
- [Amnesty : la surveillance de masse (en)](https://www.amnesty.org.uk/issues/Mass-surveillance)
- [Le Monde : l'UE et l'optimisation fiscale](http://www.lemonde.fr/economie/article/2014/06/11/optimisation-fiscale-l-ue-s-appreterait-a-lancer-une-enquete-sur-3-pays_4435744_3234.html)
- [Le Figaro : redressement fiscal pour Google](http://www.lefigaro.fr/societes/2014/02/04/20005-20140204ARTFIG00426-google-redresse-d-un-milliard-d-euros-par-le-fisc.php)
- [Webrankinfo : chiffres clés sur Google](http://www.webrankinfo.com/dossiers/google/chiffres-cles)
- [Webrankinfo : résultats financiers de Google](http://www.webrankinfo.com/dossiers/google/resultats-financiers)
- [Framablog : censure par Google](https://framablog.org/2016/10/31/si-google-vous-ignore-votre-projet-est-en-peril/)
- [Le Figaro : évasion fiscale par Google](http://www.lefigaro.fr/vox/economie/2015/08/17/31007-20150817ARTFIG00227-evasion-fiscale-comment-google-et-les-transnationales-jouent-avec-les-frontieres.php)
- [Silicon.fr : bilan de l'évasion fiscale](http://www.silicon.fr/evasion-fiscale-google-apple-ibm-microsoft-bilan-145615.html)
- [Journal du Net: google shopping](http://www.journaldunet.com/ebusiness/commerce/1193829-comment-google-shopping-a-lamine-les-comparateurs-de-prix-en-france/)


## La suite

Je compte relancer cette campagne de boycott un jour où i-boycott aura plus de participants. En attendant, vous pouvez consulter ma page [informatique durable](https://hugo.trentesaux.fr/informatique-durable/), section Google, pour en apprendre plus sur les alternatives.
