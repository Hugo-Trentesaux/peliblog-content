Title: Logiciel libre
Date: 2017-09-01
Slug: logiciel-libre
Authors: Hugo Trentesaux
Summary: 
Status: draft

# Le logiciel libre

Qu'est-ce que le logiciel libre ? C'est beaucoup de choses à la fois, et je vais le définir en abordant plusieurs angles de vue.

- une démarche de consommation responsable, au même titre que les produits bio, locaux, équitables
- une réponse apportée par des associations de défense de la vie privée pour la lutte contre la surveillance par des multinationales
- la concrétisation de valeurs comme le partage des connaissances, la solidarité, le savoir vivre ensemble, 