Title: Faire tourner un site wordpress en local
Date: 2018-05-01
Authors: Hugo Trentesaux
Summary: Faire tourner un site wordpress en local, exemple avec le site de climat-2020.eu

Aujourd'hui, j'ai entrepris d'éditer en local le site wordpress de climat-2020.eu. J'ai retenu quelques astuces qui seront utiles pour toute autre tâche similaire dans le futur. Cette démarche permet de faire tourner un site en local simplement et *sans aucun outil particulier*. Elle peut donc être appliquée à d'autres CMS.

## Télécharger les fichiers du site et sa base de donnée

Je me suis connecté en FTP pour récupérer le contenu du site. J'ai exporté la base de données mysql dans phpmyadmin, mais ça dépend d'où est installé le site que vous voulez faire tourner. Bien sûr, il vous faut les identifiants.

## Préparer son environnement de développement

### Installer les programmes nécessaires

J'ai choisi Ubuntu, parce que debian est mon environnement habituel pour les serveurs. Pour servir le site en local, j'ai choisi apache. J'aurais pu installer tous les paquets d'un coup avec lamp ou xamp, mais j'ai choisi d'installer les paquets un par un.

	sudo apt install apache2 php mysql-server libapache2-mod-php php-mysql 

C'est à dire :

- apache2 : le programme apache (alternative nginx)
- php : le langage php
- mysql-server : un logiciel de base de donnée (alternative mariadb)
- libapache2-mod-php : le module apache pour le php
- php-mysql : le module php pour mysql

### Configurer les programmes

#### Apache

Installez et activez les modules si nécessaire. Par exemple, pour activer "mod_rewrite"

	sudo a2enmod rewrite

Il faut autoriser apache à servir les fichiers du site. Comme le site redirige en https par défaut, j'ai configuré un virtualhost sur le port 443 :

    <IfModule mod_ssl.c>
        <VirtualHost _default_:443>
            DocumentRoot /home/dev/finance-climat/www
            RewriteEngine on
            SSLEngine on
            SSLCertificateFile	/etc/ssl/certs/ssl-cert-snakeoil.pem
            SSLCertificateKeyFile /etc/ssl/private/ssl-cert-snakeoil.key
        </VirtualHost>
    </IfModule>

Et dans le fichier /etc/apache2/apache2.conf

    <Directory /home/dev>
            Options Indexes FollowSymLinks
            AllowOverride None
            Require all granted
    </Directory>

Après des modifs sur les fichiers de configuration n'oubliez pas de redémarrer apache.

	sudo service apache2 restart

Vous remarquerez ici que je n'ai pas installé de certificat. J'ai juste ajouté une exception de sécurité pour ce site sur firefox, ce qui n'est pas gênant puisque je ne le consulte qu'en local.

#### Mysql

Wordpress utilise une base de données. Pour voir les paramètres, allez voir dans le fichier wp-config.php.

    define('DB_NAME', 'superdatabase');
    define('DB_USER', 'superusername');
    define('DB_PASSWORD', 'superpassword');

Changez l'adresse de la base de données à 'localhost'.

    // define('DB_HOST', 'superdatabase.sql.online.net');
    define('DB_HOST', 'localhost');

Pour créer la base de données et connecter wordpress :

se connecter à la base de données :

	mysql -u root -p

une fois dans mysql, créer l'utilisateur

	create user superusername identified by superpassword;

créez la base de données

	create database superdatabase;

donnez les droits à l'utilisateur

	grant all on superdatabase to superusername@localhost

importez la base de données depuis un terminal

	mysql -u superusername -p superdatabase < superdatabase.sql

#### Rediriger le nom de domaine vers localhost

Comme beaucoup de sites sont mal foutus et ont des liens absolus au lieu de liens relatifs, j'ai décidé d'ajouter une entrée au fichier /etc/hosts :

	# added temporarly to edit the site locally
	127.0.0.1	climat-2020.eu

Bien entendu, je ne peux plus consulter le site réel pendant que j'ai cette modification, mais c'est juste pour le développement, donc temporaire.

## Se rendre sur le site

Quand vous ouvrez votre navigateur à l'URL 'https://climat-2020.eu', après avoir ajouté une exception de sécurité (c'est en localhost, pas de certificat), vous pouvez essayer de modifier le site. Pour bien vous convaincre que c'est le site local et pas distant (avec l'URL, on pourrait s'y méprendre) arrêtez apache :

	sudo service apache2 stop

Le tour est joué, vous pouvez même consulter le site modifié depuis une autre machine en modifiant son fichier hosts avec l'adresse IP de votre machine !