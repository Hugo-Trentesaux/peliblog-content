Title: Pelican memo
Date: 2017-05-12
Tags: pelican
Slug: pelican-memo
Summary: Memo pour publier sur pelican

Quelques commandes à retenir sur pelican :

Pour générer le contenu à partir des fichiers markdown :

	pelican content

Pour prévisualiser le site en local (port 8000) :

	cd output
    python -m http.server

Condensé, cela donne :

	cd .. && pelican content && cd output && python3 -m http.server

Pour renouveler les fichier statiques sur le serveur :

	rsync -avc --delete output/ trentesaux.fr:/var/www/peliblog/

La documentation est disponible sur [http://docs.getpelican.com/en/stable](http://docs.getpelican.com/en/stable)