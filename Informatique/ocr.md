Title: Un peu d'OCR
Date: 2018-03-17
Authors: Hugo Trentesaux
Summary: Test de ocrad et gocr.

Ça fait un moment que je voulais tester des logiciels libre de reconnaissance de caractère (OCR = optical caracter recognition). J'avais déjà posé la question sur diaspora (enfin, il me semble, je n'ai pas retrouvé le post), et retenu quelques noms comme 'ocrad', 'gocr', 'tesseract'... J'ai décidé aujourd'hui d'en essayer quelques uns. J'ai tapé 'OCR' sur pamac, et téléchargé les trois premiers.

Le document de base est en pgm, voici un aperçu en png.

![sonnet]({static}/images/sonnet.png)

## ocrad

	:::bash
	ocrad sonnet.pgm -o sonnet.txt -c ascii

<pre>
SONNET

Moi, lorsque je m'observe, eh bien je n'y vois goutte.
J'ai pris ce tic de ressentir si haut et fort
Que bien souvent je me fourvoie lorsque je sors
Des sensations senties ahn que je les goUte.

Je hume l'air etje deguste les liqueurs :
C'est la etre existant a ma propre faSon,
Et j'ignore toujours quelle est la conclusion
Des sensations que je conSois contre mon c_ur.

Au fait je n'ai jamais cherche a calculer
Sije sens bien ce que je sens, s'il est pDssible
Que je sois tel et quel je semble en verite,

Si je me juge moi en usant du bon crible. |
Devant les sensations je suis un peu athee :
Est-ce moi qui ressens en moi? Manque la cle.
</pre>

## gocr

	:::bash
	gocr sonnet.pgm
    
<pre>
	trying pxH-fix by Hxp -25 -19   0   5
#	fixed by Hxp to   37   8  27  32
#SONNET

Moi, lorsque je m'observe, eh bien je n'y vois goutte.
_'ai pris ce tic de ressentir si haut. et fort
Que bien souvent je me fourvoie 1orsque je sors
Des sensations senties a_n que je les goûte.

_e hume l'air et je déguste les liqueurs:
C'est l_ être existant à ma propre façon,
Et j'ignore toujours quel1e est la conclusion
De$ sensations que je conçois contre mon ccie_ur.

Au fait je n'ai jamais cherché 4 c4lcuIer
Si je sens bien ce que je sens, s'il est possibIe
Q4e je sois te1 et quel je ?_emble en vérité,

Si je me j4ge moi en u$ant du bon crible.  '
Devant les sensations je suis un. peu athée;
Est-ce moi qui ressens en moi ? Manqu'e la clé.
</pre>

## tesseract

On note la possibilité d'indiquer la langue, et donc de trouver les caractères accentués français. Des sauts de ligne inexistants ont été ajoutés.

	:::bash
    tesseract sonnet.pgm stdout -l fra

<pre>
SONNET

Moi, lorsque je m’observe, eh bien je n'y vois goutte.
J'ai pris ce tic de ressentir si haut et fort

Que bien souvent je me fourvoie lorsque je sors
Des sensations senties afin que je les goûte.

Je hume l'air et je déguste les liqueurs :

C’est là être existant à ma propre façon,

Et j'ignore toujours quelle est la conclusion

Des sensations que je conçois contre mon cœur.

Au fait je n'ai jamais cherché à calculer
Si je sens bien ce que je sens, s'il est possible
Que je sois tel et quel je semble en vérité,

Si je me juge moi en usant du bon crible.
Devant les sensations je suis un peu athée :
Est—ce moi qui ressens en moi? Manque la Clé.
</pre>